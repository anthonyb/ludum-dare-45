package com.anthony.ld45;

import java.awt.event.KeyEvent;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.input.Keyboard;
import com.anthony.engineapi.level.Level;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.screen.Screen;
import com.anthony.engineapi.utils.Timer;
import com.anthony.ld45.entities.AchievementManager;
import com.anthony.ld45.entities.PlayerEntity;
import com.anthony.ld45.entities.mobs.Moon;
import com.anthony.ld45.entities.spawner.Spawner;
import com.anthony.ld45.level.map.MainMenu;

public class MoonGame extends Game {
	
	public static final int TICKS_PER_SECOND = 25;
	
	public Moon moon;
	
	public PlayerEntity playerEntity;
	
	public AchievementManager achievementManager;
	
	private Timer fullscreenTimer;
	
	public MoonGame() {
		
		super(800, 450, 1, "The Story of You");
		
//		super.setLevel(new Level(20, 20, new Camera(this), new SpaceMap()));
		super.setLevel(new Level(0, 0, new Camera(this), new MainMenu()));
		
		this.achievementManager = new AchievementManager(this);
		
		super.setMaxTPS(MoonGame.TICKS_PER_SECOND);
		
		this.fullscreenTimer = new Timer();
	}
	
	@Override
	public void _render(Screen screen) {
	}
//	Timer pressTimer = new Timer();
	@Override
	public void _tick() {
		
//		System.out.println(super.level.getEntities().size());
		this.fullscreenTimer.tick();
		
		if (this.fullscreenTimer.ready()) {
			
			if (Keyboard.getPressed(KeyEvent.VK_O) && !super.fullscreen) {
				
				super.setFullscreen(true);
				this.fullscreenTimer.set(MoonGame.TICKS_PER_SECOND);
			} else if (Keyboard.getPressed(KeyEvent.VK_ESCAPE) && super.fullscreen) {
				
				super.setFullscreen(false);
				this.fullscreenTimer.set(MoonGame.TICKS_PER_SECOND);
			}
		}
//		
//////		TESTING:
//		pressTimer.tick();
//		if (pressTimer.ready() && Keyboard.getPressed(KeyEvent.VK_W)) {
//			
//			Ship ship = new Ship(super.getWidth() / 2 + 50, super.getHeight() / 2, 0, -1, true, this);
//
//			super.level.addEntity(ship);
//			pressTimer.set(TICKS_PER_SECOND);
//		}
//		if (pressTimer.ready() && Keyboard.getPressed(KeyEvent.VK_S)) {
//			
//			Ship ship = new Ship(super.getWidth() / 2, super.getHeight() / 2, 0, 1, true, this);
//
//			super.level.addEntity(ship);
//			pressTimer.set(TICKS_PER_SECOND);
//		}
//		if (pressTimer.ready() && Keyboard.getPressed(KeyEvent.VK_A)) {
//			
//			Ship ship = new Ship(super.getWidth() / 2 - 150, super.getHeight() / 2, 0, -1, true, this);
//
//			super.level.addEntity(ship);
//			pressTimer.set(TICKS_PER_SECOND);
//		}
//		if (pressTimer.ready() && Keyboard.getPressed(KeyEvent.VK_S)) {
//			
//			Ship ship = new Ship(super.getWidth() / 2, super.getHeight() / 2, 0, 1, this);
//
//			super.level.addEntity(ship);
//			pressTimer.set(TICKS_PER_SECOND);
//		}
//		if (pressTimer.ready() && Keyboard.getPressed(KeyEvent.VK_A)) {
//			
//			Ship ship = new Ship(super.getWidth() / 2, super.getHeight() / 2, -1, 0, this);
//
//			super.level.addEntity(ship);
//			pressTimer.set(TICKS_PER_SECOND);
//		}
//		if (pressTimer.ready() && Keyboard.getPressed(KeyEvent.VK_D)) {
//			
//			Ship ship = new Ship(super.getWidth() / 2, super.getHeight() / 2, 1, 0, this);
//
//			super.level.addEntity(ship);
//			pressTimer.set(TICKS_PER_SECOND);
//		}
	}
	
	@Override
	public void setLevel(Level level) {
		
		if (!level.getEntities().contains(this.achievementManager)) {
			
			level.addEntity(this.achievementManager);
		}
		
		super.setLevel(level);
	}
	
	public void startGame() {
		
		Spawner spawner = new Spawner(this);
		
		Main.instance.playerEntity = new PlayerEntity(this);
		
		
		super.level.addEntity(spawner);
		super.level.addEntity(playerEntity);
	}
}
