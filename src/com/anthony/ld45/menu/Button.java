/**
 * @author Pranshu Teotia
 */

package com.anthony.ld45.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;

import com.anthony.engineapi.input.Mouse;
import com.anthony.engineapi.screen.Screen;

public class Button extends Rectangle{
	
	private String value;
	private Color color;
	private Font font;
	
	public Button(String _value, int _width, int _height, Color _color, Font _font) {
		super(0,0,_width, _height);
		this.value = _value;
		this.color = _color;	
		this.font = _font;
	}
	
	public Button(String _value, int _x, int _y, int _width, int _height, Color _color, Font _font) {
		super(_x, _y, _width, _height);
		this.value = _value;
		this.color = _color;
		this.font = _font;
	}
	
	public boolean checkMousePress() {
		
		if(Mouse.getPressed(Mouse.MOUSE_LEFT_BUTTON_CODE)) {
			
			if(Mouse.POSITION.x > this.x && Mouse.POSITION.x < (this.width+this.x)) {
				
				if(Mouse.POSITION.y > this.y && Mouse.POSITION.y < (this.height+this.y)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public void setPosition(int _x, int _y) {
		this.x = _x;
		this.y = _y;	
	}
	
	public String getValue() {
		return this.value;
	}
	
	public void render(Screen screen) {
		
		screen.getGraphics().setFont(this.font);
		float title_len = (screen.getGraphics().getFontMetrics().stringWidth(this.value))/2;
		
		screen.renderRect(this.getBounds(), true, this.color, false);
		screen.renderText(this.value, (this.x+(this.width/2)-title_len), (this.y+(3*this.height)/4), this.font, Color.WHITE, false);
	}
	
	public void render(Screen screen, Color color) {
		
		screen.getGraphics().setFont(this.font);
		float title_len = (screen.getGraphics().getFontMetrics().stringWidth(this.value))/2;
		
		screen.renderRect(this.getBounds(), true, color, false);
		screen.renderText(this.value, (this.x+(this.width/2)-title_len), (this.y+(3*this.height)/4), this.font, Color.WHITE, false);
	}

}
