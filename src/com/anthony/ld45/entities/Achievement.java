package com.anthony.ld45.entities;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Stroke;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.screen.Screen;
import com.anthony.engineapi.utils.Timer;
import com.anthony.ld45.MoonGame;

public class Achievement {
	
	public static final int BANNER_LENGTH = 315,
							BANNER_HEIGHT = 75,
							BANNER_STROKE_WEIGHT = 5,
							BANNER_SPEED = 5
							;
	
	public static final BasicStroke BANNER_STROKE = new BasicStroke(Achievement.BANNER_STROKE_WEIGHT);
	
	public static final Font 	BIG_FONT = new Font("Verdana", 0, 25),
								SMALL_FONT = new Font("Verdana", 0, 12)
								;
	
	private boolean done, descending;
	
	private int seconds, xPos, yPos;
	
	private String[] text;
	
	private Timer stayTimer;
	
	public Achievement(int seconds, Game game, String... text) {
		
		this.done = false;
		this.descending = false;
		
		this.seconds = seconds;
		this.xPos = game.getWidth() - Achievement.BANNER_LENGTH - Achievement.BANNER_STROKE_WEIGHT / 2;
		this.yPos = Achievement.BANNER_STROKE_WEIGHT / 2 - Achievement.BANNER_HEIGHT;
		
		this.text = text;
		
		this.stayTimer = new Timer();
	}
	
	public void tick(Camera camera) {
		
		if (!this.descending) {
			
			this.yPos += Achievement.BANNER_SPEED;
			
			if (this.yPos >= 0) {
				
				this.descending = true;
				this.stayTimer.set(this.seconds * MoonGame.TICKS_PER_SECOND);
			}
		} else {
			
			this.stayTimer.tick();
			
			if (this.stayTimer.ready()) {
				
				this.yPos -= Achievement.BANNER_SPEED;
				
				if (this.yPos <= -Achievement.BANNER_HEIGHT) {
					
					this.done = true;
				}
			}
		}
	}
	
	public void render(Screen screen) {
		
		Graphics2D g = screen.getGraphics();
		g.setColor(Color.DARK_GRAY);
		g.fillRoundRect(this.xPos, this.yPos, Achievement.BANNER_LENGTH, Achievement.BANNER_HEIGHT, 25, 25);
		g.setColor(Color.GRAY);
		Stroke originalStroke = g.getStroke();
		g.setStroke(Achievement.BANNER_STROKE);
		g.drawRoundRect(this.xPos, this.yPos, Achievement.BANNER_LENGTH, Achievement.BANNER_HEIGHT, 25, 25);
		g.setStroke(originalStroke);
		
		screen.renderText(this.text[0], this.xPos + 12, this.yPos + 32, Achievement.BIG_FONT, Color.BLACK, false);
		screen.renderText(this.text[0], this.xPos + 10, this.yPos + 30, Achievement.BIG_FONT, Color.GREEN, false);
		
		for (int i = 1; i < this.text.length; i++) {
			
			String line = this.text[i];
			
			screen.renderText(line, this.xPos + 12, this.yPos + 47 + ((i - 1) * Achievement.SMALL_FONT.getSize()), Achievement.SMALL_FONT, Color.BLACK, false);
			screen.renderText(line, this.xPos + 10, this.yPos + 45 + ((i - 1) * Achievement.SMALL_FONT.getSize()), Achievement.SMALL_FONT, Color.WHITE, false);
		}
		
		
//		screen.renderRect(super.position, Achievement.BANNER_LENGTH, Achievement.BANNER_HEIGHT, true, Color.DARK_GRAY, false);
		
		//No super.render(screen) since we're using an null sprite anyways.
	}
	
	public boolean getDone() {
		
		return this.done;
	}
}
