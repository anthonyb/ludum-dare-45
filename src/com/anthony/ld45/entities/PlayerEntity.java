package com.anthony.ld45.entities;

import java.awt.Color;
import java.awt.Font;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.collision.separatingaxistheorem.shape.CircleCollider;
import com.anthony.engineapi.entity.ImageEntity;
import com.anthony.engineapi.input.Mouse;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.screen.Screen;
import com.anthony.engineapi.screen.sprite.Sprite;
import com.anthony.engineapi.utils.Timer;
import com.anthony.ld45.Main;
import com.anthony.ld45.MoonGame;
import com.anthony.ld45.entities.mobs.Star;

public class PlayerEntity extends ImageEntity {
	
	public static final int MAX_STARS = 4;
	
	public static final Font 	POINTS_FONT = new Font("Courier New", 1, 22),
								REMOVE_STAR_FONT = new Font("Verdana", 1, 12),
								STARS_LEFT_FONT = new Font("Helvetica", 0, 12)
								;
	
	private boolean canPlace, hoverDeleteStar, hoverMoon, blockMousePressUntilAgain, starPlaceAchievement;
	
	private int stars;
	
	private Timer actionTimer;
	
	public PlayerEntity(Game game) {
		
		super(0, 0, Sprite.NULL_SPRITE, game);
		
		super.persistent = true;
		
		this.canPlace = false;
		this.hoverDeleteStar = false;
		this.hoverMoon = false;
		this.blockMousePressUntilAgain = false;
		this.starPlaceAchievement = false;
		
		this.stars = PlayerEntity.MAX_STARS;
		
		actionTimer = new Timer();
	}

	@Override
	public void tick(Camera camera) {
		
		actionTimer.tick();
		
		boolean mouseLeftClick = Mouse.getPressed(Mouse.MOUSE_LEFT_BUTTON_CODE);

		if (this.blockMousePressUntilAgain && !mouseLeftClick) {
			
			this.blockMousePressUntilAgain = false;
		}
		
		this.canPlace = true;
		this.hoverDeleteStar = false;
		this.hoverMoon = false;
		
		Star removeStar = null;
		
		for (Star s : Star.stars) {
			
//				The star's radius + an initial radius for the new star that will be placed.
			int minDist = s.getGravityRadius() + Star.INITIAL_GRAVITY_RADIUS;
			
			if (s.distanceSquared(Mouse.POSITION) <= minDist * minDist) {
				
				float radius = ((CircleCollider) s.collider).getRadius();
				
				if (s.distanceSquared(Mouse.POSITION) < radius * radius) {
					
					if (mouseLeftClick) {
						
						removeStar = s;
					}
					
					this.hoverDeleteStar = true;
				}
				
				this.canPlace = false;
//				Can't break - this is so we can check if hovering over to delete another star that's nearby.
			}
			
		}
		
		
		float moonToMouseDistanceSquared = Main.instance.moon.distanceSquared(Mouse.POSITION);
//		Check if too close to the moon as well.
		int minDist = Star.MAX_GRAVITY_RADIUS - Star.ORBIT_RADIUS;
		if (moonToMouseDistanceSquared <  minDist * minDist) {
			
			this.canPlace = false;
		}
		
		float moonRadius = ((CircleCollider) Main.instance.moon.collider).getRadius();
		if (moonToMouseDistanceSquared < moonRadius * moonRadius) {
			
			this.hoverMoon = true;
		}
		
		if (!this.blockMousePressUntilAgain && mouseLeftClick) {
			
//				System.out.println(mousePos);
				
			if (this.canPlace && this.stars > 0 && this.actionTimer.ready()) {
				
				if (!this.starPlaceAchievement) {
					
					this.starPlaceAchievement = true;
					Main.instance.achievementManager.addAchievement(new Achievement(5, super.game, "Achievement Get!", "A gravity field appeared out of thin air...", "What\'s going on?"));
				}
									
				Star star = new Star((int) Mouse.POSITION.x, (int) Mouse.POSITION.y, super.game);
				super.game.getLevel().addEntity(star);
				this.stars--;
				this.blockMousePressUntilAgain = true;
			} else if (removeStar != null) {

				super.game.getLevel().addEntity(new Explosion((int) removeStar.position.x, (int) removeStar.position.y, super.game));
				super.game.getLevel().removeEntity(removeStar);
				Star.stars.remove(removeStar);
				removeStar.launchShips();
				this.stars++;
				actionTimer.set((int) (0.5*MoonGame.TICKS_PER_SECOND));
			}
		}
	}
	
	@Override
	public void render(Screen screen) {
		
		if (this.hoverMoon) {
			
			if (!Main.instance.moon.getShowingPoints()) {
				
				int points = Main.instance.moon.getPoints();
				String pointsTxt = "" + points;
				int stringWidth = screen.getGraphics().getFontMetrics(PlayerEntity.POINTS_FONT).stringWidth(pointsTxt);
				screen.renderText(pointsTxt, Mouse.POSITION.x - stringWidth / 2 + 1, Mouse.POSITION.y + 1, PlayerEntity.POINTS_FONT, Color.BLACK, false);
				screen.renderText(pointsTxt, Mouse.POSITION.x - stringWidth / 2, Mouse.POSITION.y, PlayerEntity.POINTS_FONT, Color.RED, false);
			}
//			Otherwise points are showing and hovering over moon, so just render nothing
		} else if (this.hoverDeleteStar) {
			
			String remove = "DESTROY";
			int stringWidth = screen.getGraphics().getFontMetrics(PlayerEntity.REMOVE_STAR_FONT).stringWidth(remove);
			screen.renderText(remove, Mouse.POSITION.x - stringWidth / 2 + 1, Mouse.POSITION.y + 1, PlayerEntity.REMOVE_STAR_FONT, Color.BLACK, false);
			screen.renderText(remove, Mouse.POSITION.x - stringWidth / 2, Mouse.POSITION.y, PlayerEntity.REMOVE_STAR_FONT, Color.RED, false);
		} else {
			
			screen.renderText(this.stars + "/" + PlayerEntity.MAX_STARS, Mouse.POSITION.x - 7, Mouse.POSITION.y, PlayerEntity.STARS_LEFT_FONT, this.stars > 0 ? Color.WHITE : Color.RED, false);
			if (this.stars > 0) {
				
				screen.renderOval(Mouse.POSITION.x - Star.INITIAL_GRAVITY_RADIUS, Mouse.POSITION.y - Star.INITIAL_GRAVITY_RADIUS, 2 * Star.INITIAL_GRAVITY_RADIUS, 2 * Star.INITIAL_GRAVITY_RADIUS, false, this.canPlace ? Color.GREEN : Color.RED, false);
			}				
		}
		
		//No super.render(screen) because we are using a null sprite anyways.
	}
	
	public void addStarToInventory() {
		
		if (this.stars < PlayerEntity.MAX_STARS) {
			
			this.stars++;
		}
	}
}
