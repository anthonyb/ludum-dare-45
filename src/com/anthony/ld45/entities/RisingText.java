package com.anthony.ld45.entities;

import java.awt.Color;
import java.awt.Font;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.entity.Entity;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.screen.Screen;

public class RisingText extends Entity {
	
	private int duration, time;
	
	private float ySpeed;
	
	private String text;
	
	private Color color;
	
	private Font font;
	
	public RisingText(String text, int x, int y, int duration, float ySpeed, Color color, Font font, Game game) {
		
		super(x, y, game);
		
		this.text = text;
		
		this.duration = duration;
		this.time = 0;
		
		this.ySpeed = ySpeed;
		
		this.color = color;
		
		this.font = font;
	}

	@Override
	public void tick(Camera camera) {
		
		if (this.time > this.duration) {
			
			super.game.getLevel().removeEntity(this);
			return;
		}
		
		this.time++;
		super.position.y -= this.ySpeed;
	}
	
	@Override
	public void render(Screen screen) {
		
		screen.renderText(this.text, super.position, this.font, this.color, false);
	}
}
