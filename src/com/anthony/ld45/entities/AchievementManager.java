package com.anthony.ld45.entities;

import java.util.ArrayList;
import java.util.List;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.entity.ImageEntity;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.screen.Screen;
import com.anthony.engineapi.screen.sprite.Sprite;
import com.anthony.ld45.statics.Statics;

public class AchievementManager extends ImageEntity {
	
	private List<Achievement> achievements;
	
	private Achievement achievementOn;
	
	public AchievementManager(Game game) {
		
		super(0, 0, Sprite.NULL_SPRITE, game);
		
		super.position.setZIndex(Statics.ACHIEVEMENT_LAYER);
		
		super.persistent = true;
		
		this.achievements = new ArrayList<Achievement>();
	}
	
	@Override
	public void tick(Camera camera) {
		
		if (this.achievementOn == null || this.achievementOn.getDone()) {
		
			if (!this.achievements.isEmpty()) {
				
				this.achievementOn = this.achievements.remove(0);
			} else {
				
				this.achievementOn = null;
			}
		}
		
		if (this.achievementOn != null) {
			
			this.achievementOn.tick(camera);
		}
	}
	
	@Override
	public void render(Screen screen) {
		
		if (this.achievementOn != null) {
			
			this.achievementOn.render(screen);
		}
		
//		no need to super.render(screen) since using a null sprite anyways.
	}

	public void addAchievement(Achievement achievement) {
		
		this.achievements.add(achievement);
	}
}
