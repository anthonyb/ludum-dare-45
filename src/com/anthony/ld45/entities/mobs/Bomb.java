package com.anthony.ld45.entities.mobs;

import java.awt.Color;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.collision.separatingaxistheorem.SeparatingAxisTheorem;
import com.anthony.engineapi.collision.separatingaxistheorem.shape.CircleCollider;
import com.anthony.engineapi.level.Level;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.math.point.Vector;
import com.anthony.engineapi.screen.Screen;
import com.anthony.ld45.Main;
import com.anthony.ld45.entities.Explosion;
import com.anthony.ld45.sprites.Sprites;

public class Bomb extends Asteroid {

	public Bomb(int x, int y, Vector direction, Game game) {
		
		super(x, y, direction, game);
		
//		Cuz it's not an asteroid
		Asteroid.asteroids.remove(this);
		
		super.sprite = Sprites.BOMB; 
		
		super.collider = new CircleCollider(0, 0, Sprites.BOMB.getWidth() / 2 - 2, this);
	}
	
	@Override
	public void tick(Camera camera) {
		
		for (Star s : Star.stars) {
			
			if (SeparatingAxisTheorem.getCollisionResult(this, s).getColliding()) {
				
//				Kill star and bomb, create explosion.
				s.launchShips();
				
				Level level = super.game.getLevel();
				
				Star.stars.remove(s);
				
				level.removeEntity(s);
				level.removeEntity(this);
				level.addEntity(new Explosion((int) s.position.x, (int) s.position.y, super.game));
				
				Main.instance.playerEntity.addStarToInventory();
				return;
			}
		}
		
		super.tick(camera);
	}
}
