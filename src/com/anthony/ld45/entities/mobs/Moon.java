package com.anthony.ld45.entities.mobs;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.collision.separatingaxistheorem.shape.CircleCollider;
import com.anthony.engineapi.entity.Mob;
import com.anthony.engineapi.level.Level;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.screen.Screen;
import com.anthony.engineapi.utils.RiseFallInt;
import com.anthony.engineapi.utils.Timer;
import com.anthony.ld45.Main;
import com.anthony.ld45.MoonGame;
import com.anthony.ld45.entities.Achievement;
import com.anthony.ld45.entities.Laser;
import com.anthony.ld45.entities.RisingText;
import com.anthony.ld45.sprites.Sprites;

//This is a planet
public class Moon extends Mob {
	
	public static final int POINTS_STEP = 11,
							SHIP_POINTS = Moon.POINTS_STEP * 10,
							GOLDEN_SHIP_POINTS = Moon.POINTS_STEP * 50
							;
			
	public static Font 	REACHED_FONT = new Font("Verdana", 0, 25),
						POINTS_FONT = new Font("Courier New", 1, 50),
						WINNER_FONT = new Font("Courier New", 1, 100)
						;
	
	private boolean firstShipAchievement, firstGoldenAchievement;
						
	private int points, pointsDisplay, shipsPointsJustGained;
	
	private float opacity;
	
	private double theta;
	
	private RisingText shipReachedRisingText;
	
	private Timer shipReachedTimer, showPointsTimer, winTimer;
	
	private RiseFallInt pointsWiggleY;
	
	public Moon(Game game) {
		
		super(game.getWidth() / 2, game.getHeight() / 2, 3, Sprites.MOON, game);
		
		super.collider = new CircleCollider(0, 0, Sprites.MOON.getWidth() / 2, this);
		
		this.firstShipAchievement = false;
		this.firstGoldenAchievement = false;
		
		this.points = 0;
		this.pointsDisplay = 0;
		this.shipsPointsJustGained = 0;
		
		this.opacity = -1;
		
		this.theta = 0;
		
		this.shipReachedRisingText = null;
		
		this.shipReachedTimer = new Timer();
		this.showPointsTimer = new Timer();
		this.winTimer = new Timer();
		
		this.pointsWiggleY = new RiseFallInt(1, -5, 5);
	}
	
	@Override
	public void tick(Camera camera) {
		
		this.shipReachedTimer.tick();
		this.showPointsTimer.tick();
		this.winTimer.tick();
		
		if (this.points >= 5_000) {
			
			if (super.sprite != Sprites.MOON3) {
				
				super.sprite = Sprites.MOON3;
				
				this.win();
			}
		} else if (this.points >= 2_500) {
			
			if (super.sprite != Sprites.MOON2) {
				
				super.sprite = Sprites.MOON2;
				
				Main.instance.achievementManager.addAchievement(new Achievement(7, super.game, "It\'s Alive!", "You made them,", "but they created this!"));
			}
		}
		
		if (this.shipReachedTimer.ready()) {
			
			this.shipsPointsJustGained = 0;
			this.shipReachedRisingText = null;
		}
		
		
		if (this.pointsDisplay < this.points) {
			
			this.pointsWiggleY.tick();
			
			this.pointsDisplay += Moon.POINTS_STEP;
			
			if (this.pointsDisplay == this.points) {
				
				this.showPointsTimer.set(MoonGame.TICKS_PER_SECOND);
			}
		}
		
		this.theta = (this.theta + 0.03) % (2 * Math.PI);
		
		super.tick(camera);
	}
	
	@Override
	public void render(Screen screen) {
		
//		Cut-scene stuff.
		if (this.opacity >= 0) {
			
			Graphics2D g = screen.getGraphics();
			
			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, this.opacity));
			
			screen.renderSpriteRotatedAroundSelf(super.sprite, super.position.x, super.position.y, super.getWidth(), super.getHeight(), this.theta, false);
			
			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
			return;
		}
		
		
//		Reached here means done with cut-scene.
		
		screen.renderSpriteRotatedAroundSelf(super.sprite, super.position.x, super.position.y, super.getWidth(), super.getHeight(), this.theta, false);

		if (!this.winTimer.ready()) {
			
			String text = "WINNER";
			
			int stringWidth = screen.getGraphics().getFontMetrics(Moon.WINNER_FONT).stringWidth(text);
			
			Color c = this.winTimer.getTime() % 4 < 2 ? Color.YELLOW : Color.GREEN;
			
			screen.renderText(text, super.position.x - stringWidth / 2, super.getMinY() - 10, Moon.WINNER_FONT, c, false);
		} else if (this.pointsDisplay < this.points) {
			
//			Showing adding points
			
			String text = "" + this.pointsDisplay;
			
			int stringWidth = screen.getGraphics().getFontMetrics(Moon.POINTS_FONT).stringWidth(text);
			int charWidth = stringWidth / text.length();
			
			
			for (int i = 0; i < text.length(); i++) {
				
				String c = "" + text.charAt(i);
				
				screen.renderText(c, super.position.x - stringWidth / 2 + (i * charWidth), super.getMinY() - 10 + (i % 2 == 0 ? 1 : -1) * this.pointsWiggleY.get(), Moon.POINTS_FONT, Color.RED, false);
			}
		} else if (!this.showPointsTimer.ready()) {

//			Showing points after
			
			String text = "" + this.pointsDisplay;
			
			int stringWidth = screen.getGraphics().getFontMetrics(Moon.POINTS_FONT).stringWidth(text);
			
			Color c = this.showPointsTimer.getTime() % 4 < 2 ? Color.RED : Color.YELLOW;
			
			screen.renderText(text, super.position.x - stringWidth / 2, super.getMinY() - 10, Moon.POINTS_FONT, c, false);
		}
	}
	
	public void addShip(Ship ship) {
//		Astronaut colliding with moon
		
		if (ship.getGolden()) {
			
			if (!this.firstGoldenAchievement) {
				
//				Main.instance.achievementManager.addAchievement(new Achievement(7, Main.instance, "Failure Get!", "You\'ve become greedy...", "You don\'t know who you are,", "but I know you\'ve changed."));
				Main.instance.achievementManager.addAchievement(new Achievement(7, Main.instance, "Achievement Get!", "You created them all equally,", "but they created their own definition of worth,", "and compared each other to themselves."));
				this.firstGoldenAchievement = true;
				System.out.println("YOURE A FAILURE");
			}
			
			this.points += Moon.GOLDEN_SHIP_POINTS;
			this.shipsPointsJustGained += Moon.GOLDEN_SHIP_POINTS;
		} else {
			
//			Don't want to trigger both achievements at once.
			if (!this.firstShipAchievement) {
				
				Main.instance.achievementManager.addAchievement(new Achievement(7, Main.instance, "Feelings Get!", "Something is tingling inside you...", "You feel a warmth from within you.", "It\'s the first time."));
				this.firstShipAchievement = true;
				System.out.println("FEELINGS ACHIEVED");
			}
			
			this.points += Moon.SHIP_POINTS;
			this.shipsPointsJustGained += Moon.SHIP_POINTS;
		}
		
		
		Level level = super.game.getLevel();
		
		if (this.shipReachedRisingText != null) {
			
			level.removeEntity(this.shipReachedRisingText);
		}
		
		this.shipReachedRisingText = new RisingText("+" + this.shipsPointsJustGained, super.getMaxX() + 30, super.getMaxY() + 50, (int) (0.5 * MoonGame.TICKS_PER_SECOND), 2.5f, Color.YELLOW, Moon.REACHED_FONT, super.game);
		level.addEntity(this.shipReachedRisingText);;
		this.shipReachedTimer.set(2 * MoonGame.TICKS_PER_SECOND);
	}
	
	public void win() {

		this.winTimer.set(5 * MoonGame.TICKS_PER_SECOND);
		this.laserShow();
	}
	
	public void laserShow() {
		
		Main.instance.achievementManager.addAchievement(new Achievement(7, super.game, "And They\'re Off!", "They don\'t need you anymore,", "but you still love them!"));
		Main.instance.achievementManager.addAchievement(new Achievement(7, super.game, "What\'s Next?", "Feel free to continue playing!"));
		
		int gameHeight = super.game.getHeight();
		Level level = super.game.getLevel();
		
		level.addEntity(new Laser(25, gameHeight, 300, Color.CYAN, super.game));
		level.addEntity(new Laser(60, gameHeight, 270,  Color.GREEN, super.game));
		level.addEntity(new Laser(100, gameHeight, 310, Color.MAGENTA, super.game));
		
		int width = super.game.getWidth();
		level.addEntity(new Laser(width - 25, gameHeight, false, 1000, 190, 180, 269, Color.RED, super.game));
		level.addEntity(new Laser(width - 60, gameHeight, false, 1000, 210, 180, 269, Color.ORANGE, super.game));
		level.addEntity(new Laser(width - 100, gameHeight, false, 1000, 240, 180, 269, Color.YELLOW, super.game));

	}
	
	public void setOpacity(float opacity) {
		
		this.opacity = opacity;
	}
	
	public boolean getReceivedFirstShipAlready() {
		
		return this.firstShipAchievement;
	}
	
	public boolean getShowingPoints() {
		
		return this.pointsDisplay < this.points || !this.showPointsTimer.ready();
	}
	
	public int getPoints() {
		
		return this.pointsDisplay;
	}
}
