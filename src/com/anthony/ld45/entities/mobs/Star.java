package com.anthony.ld45.entities.mobs;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.collision.separatingaxistheorem.shape.CircleCollider;
import com.anthony.engineapi.entity.Mob;
import com.anthony.engineapi.level.Level;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.screen.Screen;
import com.anthony.engineapi.utils.Timer;
import com.anthony.ld45.Main;
import com.anthony.ld45.MoonGame;
import com.anthony.ld45.entities.Achievement;
import com.anthony.ld45.entities.RisingText;
import com.anthony.ld45.sprites.Sprites;
import com.anthony.ld45.statics.Statics;

public class Star extends Mob {
//	The thing you place to zucc
//	Cant place starts close enough to each other so the radius's overlap
	
	public static final int INITIAL_GRAVITY_RADIUS = 55,//75,
							MAX_GRAVITY_RADIUS = 125,
							ORBIT_RADIUS = 37
							;
	
	public static final Font GAINED_FONT = new Font("Verdana", 0, 12);
	
	private static boolean reachedTenAchievement = false;
	
	private int shipsGained, gravityRadius;
	
	private RisingText shipAddRisingText;
	
	private Timer shipAddTimer;
	
	public static List<Star> stars = new ArrayList<>();
	
	private List<Ship> orbiting;
	
	public Star(int x, int y, Game game) {
		
		super(x, y, 1, Sprites.STAR, game);
		
		super.position.setZIndex(Statics.STAR_LAYER);
		
		super.collider = new CircleCollider(0, 0, Sprites.STAR.getWidth() / 2, this);
		
		this.shipsGained = 0;
		this.gravityRadius = Star.INITIAL_GRAVITY_RADIUS;
		
		this.shipAddRisingText = null;
		
		this.shipAddTimer = new Timer();
		
		this.orbiting = new ArrayList<Ship>();
		
		Star.stars.add(this);
		
	}
	
	@Override
	public void tick(Camera camera) {
//		counter++;
		
		int orbiters = this.orbiting.size();
		
		if (!Star.reachedTenAchievement && orbiters >= 10) {
			
			Main.instance.achievementManager.addAchievement(new Achievement(8, Main.instance, "Failure Get: Greed!", "You don\'t know who you are,", "but I know you\'ve changed.", "Quit fooling around and guide them!"));
			Star.reachedTenAchievement = true;
		}
		
		this.gravityRadius = Star.INITIAL_GRAVITY_RADIUS + 10 * Math.max(orbiters - 1, 0);
		
		if (this.gravityRadius > Star.MAX_GRAVITY_RADIUS) {
			
			this.gravityRadius = Star.MAX_GRAVITY_RADIUS;
		}
		
		this.shipAddTimer.tick();
		
		if (this.shipAddTimer.ready()) {
			
			this.shipsGained = 0;
			this.shipAddRisingText = null;
		}
		
		super.tick(camera);
	}
	
	@Override
	public void render(Screen screen) {
		
		super.render(screen);
		
		Graphics2D gOld = (Graphics2D) screen.getGraphics().create();
		
		Graphics2D g = screen.getGraphics();
		
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4f));
		
		screen.renderOval(super.position.x - this.gravityRadius, super.position.y - this.gravityRadius, 2 * this.gravityRadius, 2 * Star.this.gravityRadius, false, Color.WHITE, false);
		
		screen.setGraphics(gOld);
//		screen.renderCollisionShape(super.collider, Color.CYAN);
		
		
//		if (this.shipsGained != 0) {
//			
//			screen.renderText("+" + this.shipsGained, super.position, Star.GAINED_FONT, Color.WHITE, false);
//		}
	}
	
	public void launchShips() {
		
		for (Ship s : this.orbiting) {
			
			s.launch(this);
		}
	}
	
	public int hasOrbiterIndex(Ship orbiter) {
		
		return this.orbiting.indexOf(orbiter);
	}
	
	public boolean hasOrbiter(Ship orbiter) {
		
		return this.orbiting.contains(orbiter);
	}
	
	public void addOrbiter(Ship orbiter) {
		
		this.orbiting.add(orbiter);
		
//		Gained an Astronaut
		this.shipsGained++;
		
		Level level = super.game.getLevel();
		
		if (this.shipAddRisingText != null) {
			
			level.removeEntity(this.shipAddRisingText);
		}
		
		this.shipAddRisingText = new RisingText("+" + this.shipsGained, (int) super.position.x, (int) super.position.y, 1 * MoonGame.TICKS_PER_SECOND, 1f, Color.WHITE, Star.GAINED_FONT, super.game);
		level.addEntity(this.shipAddRisingText);
		this.shipAddTimer.set(2 * MoonGame.TICKS_PER_SECOND);
	}
	
	public void removeOrbiter(int index) {
		
		this.orbiting.remove(index);
	}
	
	public int getGravityRadius() {
		
		return this.gravityRadius;
	}
}
