package com.anthony.ld45.entities.mobs;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.collision.separatingaxistheorem.SeparatingAxisTheorem;
import com.anthony.engineapi.collision.separatingaxistheorem.shape.CircleCollider;
import com.anthony.engineapi.entity.Mob;
import com.anthony.engineapi.level.Level;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.math.point.Vector;
import com.anthony.engineapi.screen.Screen;
import com.anthony.ld45.Main;
import com.anthony.ld45.entities.Explosion;
import com.anthony.ld45.sprites.Sprites;
import com.anthony.ld45.statics.Statics;

//This is an astronaut
public class Ship extends Mob {
	
	private boolean clockwise, golden;
	
	private int degrees, speed;
	
	private float xDir, yDir;
	
	public Ship(int x, int y, float xDir, float yDir, boolean golden, Game game) {
		
		super(x, y, 10, golden ? Sprites.SHIP_GOLDEN : Sprites.SHIP, game);
		
		if (golden) {
			
			super.position.setZIndex(Statics.GOLDEN_LAYER);
		}
		
		this.clockwise = false;
		this.golden = golden;
		
		this.degrees = (int) (Math.random() * Camera.COS.length);
		this.speed = 4;
		
		this.xDir = xDir * this.speed;
		this.yDir = yDir * this.speed;
		
		super.collider = new CircleCollider(0, 0, 10, this);
	}
	
	public void tick(Camera camera) {
		
		if (SeparatingAxisTheorem.getCollisionResult(this, Main.instance.moon).getColliding()) {
			
			Main.instance.moon.addShip(this);
			super.game.getLevel().removeEntity(this);
			return;
		}
		
		for (Asteroid asteroid : Asteroid.asteroids) {
			
			if (SeparatingAxisTheorem.getCollisionResult(this, asteroid).getColliding()) {
				
//				Destroy both and add explosion
				
				Level level = super.game.getLevel();
				
				for (Star star : Star.stars) {
					
					int orbiterIndex = star.hasOrbiterIndex(this);
					if (orbiterIndex != -1) {
						
						star.removeOrbiter(orbiterIndex);
						break;
					}
				}
				
				level.removeEntity(this);
				level.removeEntity(asteroid);
				Asteroid.asteroids.remove(asteroid);
				level.addEntity(new Explosion((int) super.position.x, (int) super.position.y, super.game));
				
				return;
			}
		}
		
		for (Star star : Star.stars) {
			
			float radius = star.getGravityRadius();
			
			float shipRadius = super.distance(star);
			
			if (shipRadius < radius) {
				
				if (!star.hasOrbiter(this)) {
					
					star.addOrbiter(this);
					
//					Create a line based on the movement Vector of the Ship from the Star's center point.
					float 	x1 = star.position.x,
							y1 = star.position.y,
							x2 = star.position.x + this.xDir,
							y2 = star.position.y + this.yDir
							;
					
//					y2 - y1 = m (x2 - x1);
//					m = (y2 - y1) / (x2 - x1);
					
					if (x1 == x2) {
						
//						This will produce Infinity for m and b because x2 - x1 is 0.

						int sign = (int) Math.signum(star.position.y - super.position.y);

						if (sign == -1 || sign == 0) {//When is sign 0?  I dont know.. but just in case we'll put it here lol.
							
							if (super.position.x > x1) {
								
								this.clockwise = false;
							} else {
								
								this.clockwise = true;
							}
						} else if (sign == 1) {
							
							if (super.position.x < x1) {
								
								this.clockwise = false;
							} else {
								
								this.clockwise = true;
							}
						}
					} else {
					
						float m = (y2 - y1) / (x2 - x1);
						
	//					y = m x + b
	//					b = y - m x
						float b = y1 - m * x1;
						
	//					gets rid of -0
	//					if (m <= 0.001f && m >= -0.001f) {
	//						m = 0f;
	//					}
						
//						System.out.println("y=" + m + "x + " + b);
						
						float predictedY = m * super.position.x + b;
						
						int xSign = (int) Math.signum(this.xDir);
						
						if (super.position.y < predictedY) {
							
//							System.out.println("POSITION Y < PREDICTED Y");
							this.clockwise = true;
						} else {
							
//							System.out.println("POSITION Y > PREDICTED Y");
							this.clockwise = false;
						}
						
						if (xSign < 0) {
							this.clockwise = !this.clockwise;
						}
//						int sign = (int) Math.signum(star.position.y - super.position.y);
//						
//						if (sign == -1 || sign == 0) {//When is sign 0?  I dont know.. but just in case we'll put it here lol.
//							System.out.println("A");
//							if (super.position.y > predictedY) {
//								
//								this.clockwise = false;
//							} else {
//								
//								this.clockwise = true;
//							}
//						} else if (sign == 1) {
//							System.out.println("B");
//							if (super.position.y < predictedY) {
//								
//								this.clockwise = false;
//							} else {
//								
//								this.clockwise = true;
//							}
//						}
					}
					
					
					
					
					this.degrees = (int) Math.toDegrees(Math.atan2(super.position.y - star.position.y, super.position.x - star.position.x));
					if (this.degrees < 0) {
						
						this.degrees += 360;
					}
					
					float startRadius = Math.max((shipRadius - 3), 0);
					
					super.position.x = star.getMidXF() + (float) (Camera.COS[this.degrees] * startRadius);
					super.position.y = star.getMidYF() + (float) (Camera.SIN[this.degrees] * startRadius);
					
					this.xDir = 0;
					this.yDir = 0;
				} else {
					
					if (shipRadius > Star.ORBIT_RADIUS) {
						
						shipRadius -= 0.5f; //Makes it orbit a little longer
					} else shipRadius = Star.ORBIT_RADIUS;
					
					if (this.clockwise) {
						
						this.degrees = (this.degrees + this.speed) % Camera.COS.length;
					} else {
						
						this.degrees = (this.degrees - this.speed) % Camera.COS.length;
						if (this.degrees < 0) {
							
							this.degrees += 360;
						}
					}
					
					super.position.x = star.getMidXF() + (float) (Camera.COS[this.degrees] * shipRadius);
					super.position.y = star.getMidYF() + (float) (Camera.SIN[this.degrees] * shipRadius);
				}
				
				break;
			}
		}
		
		super.move(this.xDir, this.yDir);
		
		if (super.position.x <= -200 || super.position.x > super.game.getWidth() + 200 ||
			super.position.y <= -200 || super.position.y > super.game.getHeight() + 200
			) {
			
//			Remove if it goes too far off screen.
			super.game.getLevel().removeEntity(this);
			return;
		}
		
		super.tick(camera);
	}
	
	@Override
	public void render(Screen screen) {
		
//		screen.renderCollisionShape(super.collider, Color.WHITE);
		
//		Vector dir = new Vector(this.xDir, this.yDir);
//		dir.multiply(15f);
//		screen.renderLine(super.position.x, super.position.y, super.position.x + dir.x, super.position.y + dir.y, Color.RED, false);
		
		screen.renderSpriteRotatedAroundSelf(super.sprite, super.position.x, super.position.y, super.getWidth(), super.getHeight(), Math.toRadians(this.degrees + 90), false);
//		super.render(screen);
	}
	
	public void launch(Star star) {
		
		float x1 = super.position.x;
		float y1 = super.position.y;


		float shipRadius = super.distance(star);
		
		int degrees = 0;
		if (this.clockwise) {
			//Degrees + 3 so it's not too short of a difference to actually get a nice tangent, but not too long either to get the wrong tangent.
			degrees = (this.degrees + 5) % Camera.COS.length; 
		} else {
			
			degrees = (this.degrees - 5) % Camera.COS.length;
			if (degrees < 0) {
				
				degrees += 360;
			}
		}
		
		float x2 = star.getMidXF() + Camera.COS[degrees] * shipRadius;
		float y2 = star.getMidYF() + Camera.SIN[degrees] * shipRadius;
		
		Vector moveDir = new Vector((x2 - x1), (y2 - y1));
		moveDir.normalize();
		
		this.xDir = 1.5f * this.speed * moveDir.x;
		this.yDir = 1.5f * this.speed * moveDir.y;
		
////		y2 - y1 = m (x2 - x1)
////		m = (y2 - y1) / (x2 - x1)
//		float m = (y2 - y1) / (x2 - x1);
//		
////		y = m * x + b;
////		b = y - m * x
//		float b = y1 - m * x1;
//		
////		now can use y = mx + b to determine direction
		
	}
	
	public boolean getGolden() {
		
		return this.golden;
	}
}