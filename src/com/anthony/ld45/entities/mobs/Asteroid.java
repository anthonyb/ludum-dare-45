package com.anthony.ld45.entities.mobs;

import java.util.ArrayList;
import java.util.List;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.collision.separatingaxistheorem.shape.CircleCollider;
import com.anthony.engineapi.entity.Mob;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.math.point.Vector;
import com.anthony.engineapi.screen.Screen;
import com.anthony.ld45.sprites.Sprites;
import com.anthony.ld45.statics.Statics;

public class Asteroid extends Mob {
	
	public static final int ASTEROID_SPEED = 5;
	
	public static List<Asteroid> asteroids = new ArrayList<Asteroid>();
	
	private float xDir, yDir;
	
	public Asteroid(int x, int y, Vector direction, Game game) {
		
		super(x, y, 1, Sprites.ASTEROID, game);
		
		super.collider = new CircleCollider(0, 0, Sprites.ASTEROID.getWidth() / 2 - 5, this);
		
		direction.normalize();
		direction.multiply(Asteroid.ASTEROID_SPEED);
		
		this.xDir = direction.x;
		this.yDir = direction.y;
		
		super.position.setZIndex(Statics.ASTEROID_LAYER);
		
		Asteroid.asteroids.add(this);
	}
	
	@Override
	public void tick(Camera camera) {
		
		if (super.position.x <= -200 || super.position.x > super.game.getWidth() + 200 ||
			super.position.y <= -200 || super.position.y > super.game.getHeight() + 200
			) {
			
//			Remove if it goes too far off screen.
			super.game.getLevel().removeEntity(this);
			Asteroid.asteroids.remove(this);
			return;
		}
		
		super.move(this.xDir, this.yDir);
		
		super.tick(camera);
	}
	
	@Override
	public void render(Screen screen) {
		
		super.render(screen);
	}
}
