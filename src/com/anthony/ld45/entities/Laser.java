package com.anthony.ld45.entities;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.entity.ImageEntity;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.screen.Screen;
import com.anthony.engineapi.screen.sprite.Sprite;

public class Laser extends ImageEntity {
	
	public static final Stroke STROKE = new BasicStroke(30);
	
	private boolean rising;
	
	private int radius;
	
	private float x2, y2;
	
	private int degrees, minDeg, maxDeg;
	
	private Color color;
	
//	Positive degrees only
	public Laser(int x, int y, boolean rising, int radius, int startDeg, int minDeg, int maxDeg, Color color, Game game) {
		
		super(x, y, Sprite.NULL_SPRITE, game);
		
		super.persistent = true;
		
		this.rising = rising;
		
		this.radius = radius;
		
		this.x2 = super.position.x;
		this.y2 = super.position.y;
		
		this.degrees = startDeg;
		this.minDeg = minDeg;
		this.maxDeg = maxDeg;
		
		this.color = color;
	}
	
	public Laser(int x, int y, int startDeg, Color color, Game game) {
		
		this(x, y, true, 1000, startDeg, 270, 359, color, game);
	}
	
	@Override
	public void tick(Camera camera) {
		
		if (this.rising) {
			
			this.degrees++;
			
			if (this.degrees >= this.maxDeg) {
				
				this.degrees = this.maxDeg;
				this.rising = false;
			}
		} else {
			
			this.degrees--;
			
			if (this.degrees <= this.minDeg) {
				
				this.degrees = this.minDeg;
				this.rising = true;
			}
		}
		
		this.x2 = super.position.x + this.radius * Camera.COS[this.degrees];
		this.y2 = (super.position.y + 20) + this.radius * Camera.SIN[this.degrees];
	}
	
	@Override
	public void render(Screen screen) {
		
		Graphics2D gOld = (Graphics2D) screen.getGraphics().create();
		
		Graphics2D g = screen.getGraphics();
		
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.2f));
		g.setStroke(Laser.STROKE);
		screen.renderLine(super.position.x, super.position.y + 20, this.x2, this.y2, this.color, false);
		
		screen.setGraphics(gOld);
	}
}
