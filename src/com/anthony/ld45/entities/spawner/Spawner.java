package com.anthony.ld45.entities.spawner;

import java.util.Random;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.entity.Entity;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.math.point.Vector;
import com.anthony.ld45.Main;
import com.anthony.ld45.entities.mobs.Asteroid;
import com.anthony.ld45.entities.mobs.Bomb;
import com.anthony.ld45.entities.mobs.Ship;
import com.anthony.ld45.sprites.Sprites;

public class Spawner extends Entity {
	
	public static final Random RANDOM = new Random();
	
	public Spawner(Game game) {
		
		super(0, 0, game);
		
		super.persistent = true;
	}

	@Override
	public void tick(Camera camera) {
		
//		1/200 chance
		if (Spawner.RANDOM.nextInt(10) == 0) {
			
			int xVal = 0, yVal = 0;
			float xDir = 0f, yDir = 0f;
			
			int side = Spawner.RANDOM.nextInt(4);

			boolean enemy = Spawner.RANDOM.nextInt(4) == 0;
			
			if (enemy) {
				
				switch (side) {
				case 0:
//					Above screen
					xVal = super.game.getWidth() / 2;
					yVal = -Sprites.SHIP.getHeight();
					xDir = Spawner.RANDOM.nextFloat() * 2 - 1;
					yDir = Spawner.RANDOM.nextFloat() * 1 - 1;
					break;
				case 1:
//					Below screen
					xVal = super.game.getWidth() / 2;
					yVal = super.game.getHeight() + Sprites.SHIP.getHeight();
					xDir = Spawner.RANDOM.nextFloat() * 2 - 1;
					yDir = Spawner.RANDOM.nextFloat() * 1;
					break;
				case 2:
//					Left of Screen
					xVal = -Sprites.SHIP.getWidth();
					yVal = super.game.getHeight() / 2;
					xDir = Spawner.RANDOM.nextFloat() * 1;
					yDir = Spawner.RANDOM.nextFloat() * 2 - 1;
					break;
				case 3:
//					Right of screen
					xVal = super.game.getWidth() + Sprites.SHIP.getWidth();
					yVal = super.game.getHeight() / 2;
					xDir = Spawner.RANDOM.nextFloat() * 1 - 1;
					yDir = Spawner.RANDOM.nextFloat() * 2 - 1;
					break;
				}
				
				boolean bomb = Spawner.RANDOM.nextInt(5) == 0;
				
				if (bomb) {
					
					Bomb b = new Bomb(xVal, yVal, new Vector(xDir, yDir), super.game);
					super.game.getLevel().addEntity(b);
				} else {
				
					Asteroid a = new Asteroid(xVal, yVal, new Vector(xDir, yDir), super.game);
					super.game.getLevel().addEntity(a);
				}
			} else {
				
				switch (side) {
				case 0:
//					Above screen
					xVal = Spawner.RANDOM.nextInt(super.game.getWidth());
					yVal = -Sprites.SHIP.getHeight();
					xDir = Spawner.RANDOM.nextFloat() * 2 - 1;
					yDir = Spawner.RANDOM.nextFloat() * 1 - 1;
					break;
				case 1:
//					Below screen
					xVal = Spawner.RANDOM.nextInt(super.game.getWidth());
					yVal = super.game.getHeight() + Sprites.SHIP.getHeight();
					xDir = Spawner.RANDOM.nextFloat() * 2 - 1;
					yDir = Spawner.RANDOM.nextFloat() * 1;
					break;
				case 2:
//					Left of Screen
					xVal = -Sprites.SHIP.getWidth();
					yVal = Spawner.RANDOM.nextInt(super.game.getHeight());
					xDir = Spawner.RANDOM.nextFloat() * 1;
					yDir = Spawner.RANDOM.nextFloat() * 2 - 1;
					break;
				case 3:
//					Right of screen
					xVal = super.game.getWidth() + Sprites.SHIP.getWidth();
					yVal = Spawner.RANDOM.nextInt(super.game.getHeight());
					xDir = Spawner.RANDOM.nextFloat() * 1 - 1;
					yDir = Spawner.RANDOM.nextFloat() * 2 - 1;
					break;
				}
				
				boolean golden = false;
			
//				Don't spawn golden one's unless the player has helped one already.
//				The achievements will make sense more like this.
				if (Main.instance.moon.getReceivedFirstShipAlready() && Spawner.RANDOM.nextInt(20) == 0) {
					
					golden = true;
				}
				
				Ship ship = new Ship(xVal, yVal, xDir, yDir, golden, super.game);
				super.game.getLevel().addEntity(ship);
			}
		}
	}
}
