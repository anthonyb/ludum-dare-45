package com.anthony.ld45.entities;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.entity.ImageEntity;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.screen.Screen;
import com.anthony.engineapi.screen.sprite.Sprite;
import com.anthony.ld45.MoonGame;
import com.anthony.ld45.sprites.Sprites;

public class Explosion extends ImageEntity {
	
	public static final Sprite[] EXPLOSION_SPRITES = new Sprite[] {
			Sprites.EXPLOSION,
			Sprites.EXPLOSION2,
			Sprites.EXPLOSION3,
			Sprites.EXPLOSION4,
			Sprites.EXPLOSION5
	};
	
	private int timer, animIndex;
	
	public Explosion(int x, int y, Game game) {
		
		super(x, y, Sprites.EXPLOSION, game);
		
		this.timer = 0;
		this.animIndex = 0;
	}
	
	@Override
	public void tick(Camera camera) {
		
		this.timer++;
		
		if (this.timer >= .05 * MoonGame.TICKS_PER_SECOND) {
			
			this.animIndex++;
			
			if (this.animIndex >= Explosion.EXPLOSION_SPRITES.length) {
				
				this.animIndex = Explosion.EXPLOSION_SPRITES.length - 1;
				super.game.getLevel().removeEntity(this);
				return;
			}
			
			this.timer = 0;
		}
		
		super.tick(camera);
	}
	
	@Override
	public void render(Screen screen) {
		
		screen.renderSprite(Explosion.EXPLOSION_SPRITES[this.animIndex], super.renderPoint, false);
	}
}
