package com.anthony.ld45.statics;

public class Statics {
	
	public static final int STAR_LAYER = -1,
							GOLDEN_LAYER = 1,
							ASTEROID_LAYER = 2,
							ACHIEVEMENT_LAYER = 3
							;
}
