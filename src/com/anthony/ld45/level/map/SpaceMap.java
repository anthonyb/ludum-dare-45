package com.anthony.ld45.level.map;

import java.awt.Color;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.input.Mouse;
import com.anthony.engineapi.level.map.Map;
import com.anthony.engineapi.screen.Screen;
import com.anthony.engineapi.screen.sprite.Sprite;
import com.anthony.engineapi.utils.Timer;
import com.anthony.ld45.Main;
import com.anthony.ld45.MoonGame;
import com.anthony.ld45.entities.Achievement;
import com.anthony.ld45.entities.mobs.Moon;
import com.anthony.ld45.sprites.Sprites;

public class SpaceMap extends Map {
	
	private static final int 	SCROLL_SPEED = 2,
								CUTSCENE_BLACKNESS_TIMER = 3 * MoonGame.TICKS_PER_SECOND,
								CUTSCENE_STARS_TIMER = 3 * MoonGame.TICKS_PER_SECOND
								;
	
	private static final Color SPACE_COLOR = new Color(0xFF06010B);
	
	private static Sprite[] backgrounds = new Sprite[] {
			
			Sprites.BACKGROUND,
			Sprites.BACKGROUND1,
			Sprites.BACKGROUND2,
			Sprites.BACKGROUND3,
			Sprites.BACKGROUND4
	};
	
//	private int spriteIndex, spriteAnimationRate;
	
	private int cutscene, displayX, spriteIdx1, spriteIdx2;
	
	private float cutsceneOpacity;
	
	private Timer cutsceneTimer;
	
	private Sprite sprite1, sprite2;
	
	public SpaceMap() {
		this.cutscene = 0;
//		this.cutscene = 4;
//		System.out.println("DONT FORGET TO SET THE CUTSCENE TIMERS TO THE PROPER VALUES WHEN THE GAME IS ALL DONE!");
//		
//		System.out.println("==CHEATS==");
//		final ScheduledExecutorService exec =Executors.newScheduledThreadPool(1);
//		
//		exec.schedule(new Runnable(){
//			@Override
//			public void run() {
//				Main.instance.moon = new Moon(Main.instance);
//				Main.instance.getLevel().addEntity(Main.instance.moon);
//				Main.instance.startGame(); 
//			}
//		}, 1, TimeUnit.SECONDS);
//		System.out.println("CHEATING FOR TESTING, REMOVE THIS STUFF LATER");

		
		this.displayX = 0;
		this.spriteIdx1 = 0;
		this.spriteIdx2 = 1;
		
		this.cutsceneOpacity = 1f;
		
		this.cutsceneTimer = new Timer();
		this.cutsceneTimer.set(SpaceMap.CUTSCENE_BLACKNESS_TIMER);
		
		this.sprite1 = SpaceMap.backgrounds[this.spriteIdx1];
		this.sprite2 = SpaceMap.backgrounds[this.spriteIdx2];
	}

	@Override
	public void tick(Game game) {
		
		if (this.cutscene > 1) {
			
			if (this.displayX > 0) {
				
				this.displayX -= 20 * SpaceMap.SCROLL_SPEED;
			} else {
				
				this.displayX -= SpaceMap.SCROLL_SPEED;
			}
			
			if (this.displayX <= -this.sprite1.getWidth()) {
				
				this.displayX = 0;
				
				this.spriteIdx1 = (this.spriteIdx1 + 1) % SpaceMap.backgrounds.length;
				this.spriteIdx2 = (this.spriteIdx2 + 1) % SpaceMap.backgrounds.length;
				
				this.sprite1 = SpaceMap.backgrounds[this.spriteIdx1];
				this.sprite2 = SpaceMap.backgrounds[this.spriteIdx2];
			}
		}
		
		if (this.cutscene < 3) {
			
			this.cutsceneTimer.tick();
			
			if (this.cutsceneOpacity < 1f) {
				
				this.cutsceneOpacity += 0.01f;
				if (this.cutsceneOpacity >= 0.5f) {
					
					this.cutsceneOpacity += 0.05f;//Speed it up now since you can pretty much see it.
				}
				this.cutsceneOpacity = Math.min(this.cutsceneOpacity, 1f);
			}
			
			switch (this.cutscene) {
			case 0:
				
//				If the timer is ready and the left mouse button is pressed
				if (this.cutsceneTimer.ready() && Mouse.getPressed(Mouse.MOUSE_LEFT_BUTTON_CODE)) {

//					Add the Moon
					Main.instance.moon = new Moon(game);
					game.getLevel().addEntity(Main.instance.moon);
					Main.instance.achievementManager.addAchievement(new Achievement(2, game, "???", "SOMETHING happened!"));
					this.cutsceneOpacity = 0f;
					
					this.cutscene = 1;
				}
				break;
			case 1:

//				If the timer is ready and the left mouse button is pressed
				if (this.cutsceneOpacity >= 1f && Mouse.getPressed(Mouse.MOUSE_LEFT_BUTTON_CODE)) {
					
					Main.instance.moon.setOpacity(-1f);
					
					this.displayX = game.getWidth();
					
					Main.instance.achievementManager.addAchievement(new Achievement(2, game, "Achievement Get!", "The Universe?"));
					
					this.cutscene = 2;
					this.cutsceneTimer.set(SpaceMap.CUTSCENE_STARS_TIMER);
				}
				break;
			case 2:
				
				if (this.cutsceneTimer.ready()) {
					
					Main.instance.startGame();
					this.cutscene = 3;
				}
				break;
			default:
				System.out.println("Should never get here!");
				System.exit(1);
				break;
			}
		}
		
//		this.spriteAnimationRate++;
//		
//		if (this.spriteAnimationRate > 10) {
//			
//			this.spriteIndex = (this.spriteIndex + 1) % ImageMap.backgrounds.length;
//			this.sprite = ImageMap.backgrounds[this.spriteIndex];
//			
//			this.spriteAnimationRate = 0;
//		}
	}
	
	@Override
	public void render(Screen screen) {
		
		if (this.cutscene <= 2) {
			
			screen.renderRect(0, 0, screen.getGame().getWidth(), screen.getGame().getHeight(), true, SpaceMap.SPACE_COLOR, false);
			
			if (this.cutscene < 2) {
				
				if (Main.instance.moon != null) {
					
					Main.instance.moon.setOpacity(this.cutsceneOpacity);
				}
				return;
			}
		}
		
		
		screen.renderSprite(this.sprite1, this.displayX, 0, false);
		screen.renderSprite(this.sprite2, this.displayX + this.sprite1.getWidth(), 0, false);
	}
	
	@Override
	public int getWidth() {
		
		return 0;
	}

	
	@Override
	public int getHeight() {
		
		return 0;
	}

	@Override
	public String getMapTypeId() {
		
		return "ImageMap";
	}

}
