package com.anthony.ld45.level.map;

import java.awt.Color;
import java.awt.Font;

import com.anthony.engineapi.Game;
import com.anthony.engineapi.level.Level;
import com.anthony.engineapi.level.camera.Camera;
import com.anthony.engineapi.level.map.Map;
import com.anthony.engineapi.math.point.Point;
import com.anthony.engineapi.screen.Screen;
import com.anthony.ld45.menu.Button;
import com.anthony.ld45.sprites.Sprites;

public class MainMenu extends Map {
	
	private static final String TITLE_VALUE = "The Story of You",
								SUBTITLE_VALUE = "(and them)"
								;
	
	private static final Color 	BACKGROUND_COLOR = new Color(0xFF06010B),
								PLAY_COLOR = new Color(0xFF4C2B64),
								QUIT_COLOR = new Color(0xFF7F006E)
								;
	
	public static final Font 	TITLE_TEXT = new Font("Helvetica", 0, 42),
								SUBTITLE_TEXT = new Font("Helvetica", 0, 20)
								;
	
	private float screenWidth;
	private float screenHeight;
	private Point titlePosition;
	
	private Button PLAY;
//	private Button HELP;
	private Button QUIT;
	
	private Point buttonSize;
	private Point buttonPosition;
	
	public MainMenu() {
		titlePosition = new Point();
		buttonPosition = new Point();
		buttonSize = new Point(130, 50); 
		
		PLAY = new Button("Play", (int)buttonSize.x, (int)buttonSize.y, MainMenu.PLAY_COLOR, TITLE_TEXT);
//		HELP = new Button("Help", (int)buttonSize.x, (int)buttonSize.y, Color.GREEN, TITLE_TEXT);
		QUIT = new Button("Quit", (int)buttonSize.x, (int)buttonSize.y, MainMenu.QUIT_COLOR, TITLE_TEXT);
	}

	@Override
	public void tick(Game game) {
		
		screenWidth = game.getWidth();
		screenHeight = game.getHeight();
		titlePosition.y = 200;//(screenHeight*percentageFromTheTop_TITLE)/100; // The title will always be x percent from the top.
		titlePosition.x = screenWidth/2;
		
		buttonPosition.y = 300;//(screenHeight*percentageFromTheTop_BUTTON)/100; // The title will always be x percent from the top.
		buttonPosition.x = screenWidth/2;
		
		if(PLAY.checkMousePress()) {
			System.out.println("PLAY");
			game.setLevel(new Level(300, 300, new Camera(game), new SpaceMap()));
		}
		
//		if(HELP.checkMousePress()) {
//			System.out.println("HELP");
//			// TODO Goto Help
//		}
		
		
		if(QUIT.checkMousePress()) {
			
			System.exit(0);
		}
		
	}

	@Override
	public void render(Screen screen) {
		
		screen.renderRect(0, 0, screen.getGame().getWidth(), screen.getGame().getHeight(), true, MainMenu.BACKGROUND_COLOR, false);
		
		float title_len = (screen.getGraphics().getFontMetrics(TITLE_TEXT).stringWidth(TITLE_VALUE))/2;		
		screen.renderText(TITLE_VALUE, titlePosition.x-title_len, titlePosition.y, TITLE_TEXT, Color.WHITE, false);
		float subtitle_len = (screen.getGraphics().getFontMetrics(SUBTITLE_TEXT).stringWidth(SUBTITLE_VALUE))/2;	
		screen.renderText(SUBTITLE_VALUE, titlePosition.x-subtitle_len, titlePosition.y + 30, SUBTITLE_TEXT, Color.WHITE, false);
		
		screen.renderSprite(Sprites.SHIP, titlePosition.x - Sprites.SHIP.getWidth() / 2, titlePosition.y + 43, false);
		
		PLAY.setPosition((int)(buttonPosition.x-PLAY.width/2), (int)buttonPosition.y);
		PLAY.render(screen);
		
//		HELP.setPosition(PLAY.x, (int)(PLAY.y+buttonSize.y+10));
//		HELP.render(screen);
		
		QUIT.setPosition(PLAY.x, (int)(PLAY.y+buttonSize.y+10));
		QUIT.render(screen);
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public String getMapTypeId() {
		// TODO Auto-generated method stub
		return "MainMenu";
	}

}
