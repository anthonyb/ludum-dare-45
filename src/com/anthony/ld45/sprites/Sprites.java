package com.anthony.ld45.sprites;

import com.anthony.engineapi.screen.sprite.Sprite;

public class Sprites {
	
	public static Sprite 	MOON = new Sprite("/moon/planet_start.png"),
							MOON2 = new Sprite("/moon/planet_mid.png"),
							MOON3 = new Sprite("/moon/planet_end.png"),
							BACKGROUND = new Sprite("/backgrounds/background.png"),
							BACKGROUND1 = new Sprite("/backgrounds/background2.png"),
							BACKGROUND2 = new Sprite("/backgrounds/background3.png"),
							BACKGROUND3 = new Sprite("/backgrounds/background4.png"),
							BACKGROUND4 = new Sprite("/backgrounds/background5.png"),
							SHIP = new Sprite("/ship/astronaut.png"),
							SHIP_GOLDEN = new Sprite("/ship/golden_astronaut.png"),
							STAR = new Sprite("/star/star.png"),
							EXPLOSION_SHEET = new Sprite("/explosion/explosion_sheet.png"),
							EXPLOSION = new Sprite(0, 0, 48, 48, Sprites.EXPLOSION_SHEET),
							EXPLOSION2 = new Sprite(48, 0, 48, 48, Sprites.EXPLOSION_SHEET),
							EXPLOSION3 = new Sprite(2 * 48, 0, 48, 48, Sprites.EXPLOSION_SHEET),
							EXPLOSION4 = new Sprite(3 * 48, 0, 48, 48, Sprites.EXPLOSION_SHEET),
							EXPLOSION5 = new Sprite(4 * 48, 0, 48, 48, Sprites.EXPLOSION_SHEET),
							ASTEROID = new Sprite("/asteroid/asteroid.png"),
							BOMB = new Sprite("/asteroid/bomb.png")
							;
}
