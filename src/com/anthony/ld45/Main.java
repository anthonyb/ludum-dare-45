package com.anthony.ld45;

public class Main {
	
	public static MoonGame instance;
	
	public static void main(String[] args) {
		
		Main.instance = new MoonGame();
		
//		Sprites.MOON.scale(2f, 2f);
		
		try {
			
			Main.instance.start();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}
